package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class Jadwal {

    private String title;
    private String type;
    private String jam;
    private String tanggal;

    public void setTitle(String title) { this.title = title; }
    public void setType(String type) { this.type = type; }
    public void setJam(String jam) { this.jam = jam; }
    public void setTanggal(String tanggal) { this.tanggal = tanggal; }

    public String getTitle() { return this.title; }
    public String getType() { return this.type; }
    public String getJam() { return this.jam; }
    public String getTanggal() { return this.tanggal; }
}
