package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AsdosDua extends Asdos {


    public AsdosDua(Asis asis) {
        this.name = "Jane Doe";

        this.asis = asis;
        this.asis.add(this);
    }

    public void update(){
        if(this.asis.getJadwalType().equals("ADPRO")){
            this.getJadwals().add(this.asis.getJadwal());
        }
    }
}
