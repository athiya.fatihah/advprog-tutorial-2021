package id.ac.ui.cs.advprog.tutorial1.observer.repository;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Jadwal;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class JadwalRepository {

    private Map<String, Jadwal> jadwals = new HashMap<>();

    public Map<String, Jadwal> getJadwal() {
        return jadwals;
    } //getJadwals

    /*public Jadwal save(Jadwal savedJadwal) {
        Jadwal existingJadwal = jadwals.get(savedJadwal.getTitle());
        if (existingJadwal == null) {
            jadwals.put(savedJadwal.getTitle(), savedJadwal);
            return savedJadwal;
        } else {
            return null;
        }
    }*/
    public Jadwal save(Jadwal savedJadwal) {
        Jadwal existingNama = jadwals.get(savedJadwal.getTitle());
        Jadwal existingJam = jadwals.get(savedJadwal.getJam());
        Jadwal existingTanggal = jadwals.get(savedJadwal.getTanggal());

        if ((existingNama == null) | (existingJam == null) | (existingTanggal == null)) {
            jadwals.put(savedJadwal.getTitle(), savedJadwal);
            jadwals.put(savedJadwal.getJam(), savedJadwal);
            jadwals.put(savedJadwal.getTanggal(), savedJadwal);

            return savedJadwal;
        } else {
            return null;
        }
    }
}
