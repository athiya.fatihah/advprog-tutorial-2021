package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AsdosTiga extends Asdos {


    public AsdosTiga(Asis asis) {
        this.name = "Jim Doe";

        this.asis = asis;
        this.asis.add(this);
    }

    public void update(){
        if(this.asis.getJadwalType().equals("TBA")){
            this.getJadwals().add(this.asis.getJadwal());
        }
    }
}