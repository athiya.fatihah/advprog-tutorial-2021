package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AsdosSatu extends Asdos {


    public AsdosSatu(Asis asis) {
        this.name = "John Doe";

        this.asis = asis;
        this.asis.add(this);
    }

    public void update(){
        if(this.asis.getJadwalType().equals("PPW")){
            this.getJadwals().add(this.asis.getJadwal());
        }
    }
}
