package id.ac.ui.cs.advprog.tutorial1.observer.controller;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Jadwal;
import id.ac.ui.cs.advprog.tutorial1.observer.service.AsisServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ObserverController {

    @Autowired
    private AsisServiceImpl asisService;

    @RequestMapping(value = "/jadwalasis", method = RequestMethod.GET)
    public String createJadwal(Model model){
        model.addAttribute("jadwal", new Jadwal());
        return "observer/asisForm";
    }

    @RequestMapping(value = "/add-jadwal", method = RequestMethod.POST)
    public String addJadwal(@ModelAttribute("jadwal") Jadwal jadwal) {
        asisService.addJadwal(jadwal);
        // redirect ke URL asistensi-list
        return "redirect:/asistensi-list";
    }

    @RequestMapping(value = "/asistensi-list", method = RequestMethod.GET)
    public String getParaAsdos(Model model){
        model.addAttribute("listAsdos", asisService.getParaAsdos());
        return "observer/asdosList";
    }
}
