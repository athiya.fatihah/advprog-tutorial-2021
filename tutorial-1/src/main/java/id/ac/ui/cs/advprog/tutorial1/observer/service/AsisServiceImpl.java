package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.JadwalRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Asdos;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AsisServiceImpl implements AsisService {

    private final JadwalRepository jadwalRepository;
    private final Asis asis;
    private final Asdos asdosSatu;
    private final Asdos asdosDua;
    private final Asdos asdosTiga;

    public AsisServiceImpl(JadwalRepository jadwalRepository) {
        this.jadwalRepository = jadwalRepository;
        this.asis = new Asis();

        this.asdosSatu = new AsdosSatu(this.asis);
        this.asdosDua = new AsdosDua(this.asis);
        this.asdosTiga = new AsdosTiga(this.asis);
    }

    public void addJadwal(Jadwal jadwal){
        this.jadwalRepository.save(jadwal);
        this.asis.addJadwal(jadwal);
    }

    public List<Asdos> getParaAsdos(){
        return this.asis.getParaAsdos();
    }
}
