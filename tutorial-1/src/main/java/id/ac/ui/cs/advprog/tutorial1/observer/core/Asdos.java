package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

public abstract class Asdos {

    protected Asis asis;
    protected String name;
    private List<Jadwal> jadwals = new ArrayList<>();

    public abstract void update();

    public String getName() {
        return this.name;
    }

    public List<Jadwal> getJadwals() {
        return this.jadwals;
    }
}
