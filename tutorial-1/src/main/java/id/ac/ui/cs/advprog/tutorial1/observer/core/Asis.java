package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

public class Asis {

    private List<Asdos> listAsdos = new ArrayList<>();
    private Jadwal jadwal;

    public void add(Asdos asdos) {
        listAsdos.add(asdos);
    }

    public void addJadwal(Jadwal jadwal) {
        this.jadwal = jadwal;
        broadcast();
    }

    public String getJadwalType () {return jadwal.getType();}

    public Jadwal getJadwal() {return jadwal;}

    public List<Asdos> getParaAsdos() {
        return listAsdos;
    }

    private void broadcast() {
        for(Asdos explorer : listAsdos){
            explorer.update();
        }
    }
}
