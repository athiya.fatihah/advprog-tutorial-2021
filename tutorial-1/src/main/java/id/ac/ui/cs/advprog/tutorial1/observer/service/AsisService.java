package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Asdos;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Jadwal;

import java.util.List;

public interface AsisService {

    void addJadwal(Jadwal jadwal);

    List<Asdos> getParaAsdos();

}
