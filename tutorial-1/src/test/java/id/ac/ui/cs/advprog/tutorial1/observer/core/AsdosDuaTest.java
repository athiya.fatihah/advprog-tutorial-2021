package id.ac.ui.cs.advprog.tutorial1.observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AsdosDuaTest {

    private AsdosDua asdosDua;

    @Mock
    private Asis asis;

    @BeforeEach
    public void setUp() {
        asdosDua = new AsdosDua(asis);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptADPROType() {
        Jadwal adpro = new Jadwal();
        adpro.setTitle("Dummy");
        adpro.setType("ADPRO");
        when(asis.getJadwalType()).thenReturn(adpro.getType());
        when(asis.getJadwal()).thenReturn(adpro);

        asdosDua.update();
        List<Jadwal> ppwList = asdosDua.getJadwals();

        assertThat(ppwList).contains(adpro);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldNotAcceptPPWType() {
        Jadwal ppw = new Jadwal();
        ppw.setTitle("Dummy");
        ppw.setType("PPW");
        when(asis.getJadwalType()).thenReturn(ppw.getType());

        asdosDua.update();
        List<Jadwal> ppwList = asdosDua.getJadwals();

        assertThat(ppwList).doesNotContain(ppw);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldNotAcceptTBAType() {
        Jadwal tba = new Jadwal();
        tba.setTitle("Dummy");
        tba.setType("TBA");
        when(asis.getJadwalType()).thenReturn(tba.getType());

        asdosDua.update();
        List<Jadwal> ppwList = asdosDua.getJadwals();

        assertThat(ppwList).doesNotContain(tba);
    }
}
