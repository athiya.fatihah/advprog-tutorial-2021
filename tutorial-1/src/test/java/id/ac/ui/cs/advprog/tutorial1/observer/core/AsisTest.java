package id.ac.ui.cs.advprog.tutorial1.observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AsisTest {

    private Asis asis;

    @BeforeEach
    public void setUp() {
        asis = new Asis();
    }

    @Test
    public void testWhenAddQuestIsCalledItShouldSetJadwalProperty() {
        Jadwal jadwal = new Jadwal();
        jadwal.setTitle("Dummy");
        jadwal.setType("ADPRO");

        asis.addJadwal(jadwal);

        assertThat(asis.getJadwal()).isSameAs(jadwal);
    }
}
