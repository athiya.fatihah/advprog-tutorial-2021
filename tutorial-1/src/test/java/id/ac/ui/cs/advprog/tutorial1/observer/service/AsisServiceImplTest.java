package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Jadwal;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.JadwalRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class AsisServiceImplTest {

    @Mock
    private JadwalRepository jadwalRepository;

    @InjectMocks
    private AsisServiceImpl asisService;

    @Test
    public void whenAddQuestIsCalledItShouldCallJadwalRepositorySave() {
        Jadwal jadwal = new Jadwal();
        jadwal.setTitle("Dummy");
        jadwal.setType("PPW");

        asisService.addJadwal(jadwal);

        verify(jadwalRepository, times(1)).save(jadwal);
    }
}
