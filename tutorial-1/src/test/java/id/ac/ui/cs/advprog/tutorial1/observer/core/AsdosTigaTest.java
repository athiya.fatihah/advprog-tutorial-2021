package id.ac.ui.cs.advprog.tutorial1.observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AsdosTigaTest {

    private AsdosTiga asdosTiga;

    @Mock
    private Asis asis;

    @BeforeEach
    public void setUp() {
        asdosTiga = new AsdosTiga(asis);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptTBAType() {
        Jadwal tba = new Jadwal();
        tba.setTitle("Dummy");
        tba.setType("TBA");
        when(asis.getJadwalType()).thenReturn(tba.getType());
        when(asis.getJadwal()).thenReturn(tba);

        asdosTiga.update();
        List<Jadwal> agileQuestList = asdosTiga.getJadwals();

        assertThat(agileQuestList).contains(tba);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldNotAcceptADPROType() {
        Jadwal adpro = new Jadwal();
        adpro.setTitle("Dummy");
        adpro.setType("ADPRO");
        when(asis.getJadwalType()).thenReturn(adpro.getType());

        asdosTiga.update();
        List<Jadwal> ppwList = asdosTiga.getJadwals();

        assertThat(ppwList).doesNotContain(adpro);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldNotAcceptPPWType() {
        Jadwal ppw = new Jadwal();
        ppw.setTitle("Dummy");
        ppw.setType("PPW");
        when(asis.getJadwalType()).thenReturn(ppw.getType());

        asdosTiga.update();
        List<Jadwal> ppwList = asdosTiga.getJadwals();

        assertThat(ppwList).doesNotContain(ppw);
    }
}
