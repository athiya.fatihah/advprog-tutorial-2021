package id.ac.ui.cs.advprog.tutorial1.observer.repository;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Jadwal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class JadwalRepositoryTest {

    private JadwalRepository jadwalRepository;

    @BeforeEach
    public void setUp() {
        jadwalRepository = new JadwalRepository();
    }

    @Test
    public void testSaveNewQuestShouldAddItToTheRepository() {
        Jadwal newJadwal = new Jadwal();
        newJadwal.setTitle("Dummy jadwal");
        newJadwal.setType("Mata Kuliah"); //PPW

        jadwalRepository.save(newJadwal);
        Map<String, Jadwal> repositoryQuests = jadwalRepository.getJadwal();

        assertThat(repositoryQuests).hasSize(1);
        assertThat(repositoryQuests).containsValue(newJadwal);
    }

    @Test
    public void testSaveExistingQuestWithGivenTitleShouldNotSaveItToTheRepository() {
        Jadwal savedJadwal = new Jadwal();
        savedJadwal.setTitle("Dummy jadwal");
        savedJadwal.setType("TBA");
        jadwalRepository.save(savedJadwal);
        Jadwal newJadwal = new Jadwal();
        newJadwal.setTitle("Dummy jadwal");
        newJadwal.setType("TBA");

        jadwalRepository.save(newJadwal);
        Map<String, Jadwal> repositoryDiJadwal = jadwalRepository.getJadwal();

        assertThat(repositoryDiJadwal).size().isNotEqualTo(2);
    }
}
