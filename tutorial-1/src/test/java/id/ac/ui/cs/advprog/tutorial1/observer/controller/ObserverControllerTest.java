package id.ac.ui.cs.advprog.tutorial1.observer.controller;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Asis;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Jadwal;
import id.ac.ui.cs.advprog.tutorial1.observer.service.AsisServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = ObserverController.class)
public class ObserverControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Asis asis;

    @MockBean
    private AsisServiceImpl asisService;

    @Test
    public void whenCreateQuestURLIsAccessedItShouldContainCorrectQuestModel() throws Exception {
        mockMvc.perform(get("/jadwalasis"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("jadwal"))
                .andExpect(view().name("observer/asisForm"));
    }

    @Test
    public void whenAddQuestURLIsAccessedItShouldCallAsisServiceAddQuest() throws Exception {
        Jadwal newQuest = new Jadwal();
        newQuest.setTitle("Dummy");
        newQuest.setType("R");

        mockMvc.perform(post("/add-jadwal")
                .flashAttr("quest", newQuest))
                .andExpect(handler().methodName("addJadwal"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/asistensi-list"));
    }

    @Test
    public void whenAsdosListURLIsAccessedItShouldContainListAsdosModel() throws Exception {
        mockMvc.perform(get("/asistensi-list"))
                .andExpect(status().isOk())
                .andExpect(view().name("observer/asdosList"))
                .andExpect(model().attributeExists("listAsdos"));
    }
}
