package id.ac.ui.cs.advprog.tutorial1.observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AsdosSatuTest {

    private AsdosSatu asdosSatu;

    @Mock
    private Asis asis;

    @BeforeEach
    public void setUp() {
        asdosSatu = new AsdosSatu(asis);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptPPWType() {
        Jadwal ppw = new Jadwal();
        ppw.setTitle("Dummy");
        ppw.setType("PPW");
        when(asis.getJadwalType()).thenReturn(ppw.getType());
        when(asis.getJadwal()).thenReturn(ppw);

        asdosSatu.update();
        List<Jadwal> ppwList = asdosSatu.getJadwals();

        assertThat(ppwList).contains(ppw);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldNotAcceptADPROType() {
        Jadwal adpro = new Jadwal();
        adpro.setTitle("Dummy");
        adpro.setType("ADPRO");
        when(asis.getJadwalType()).thenReturn(adpro.getType());

        asdosSatu.update();
        List<Jadwal> ppwList = asdosSatu.getJadwals();

        assertThat(ppwList).doesNotContain(adpro);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldNotAcceptTBAType() {
        Jadwal tba = new Jadwal();
        tba.setTitle("Dummy");
        tba.setType("TBA");
        when(asis.getJadwalType()).thenReturn(tba.getType());

        asdosSatu.update();
        List<Jadwal> ppwList = asdosSatu.getJadwals();

        assertThat(ppwList).doesNotContain(tba);
    }
}
